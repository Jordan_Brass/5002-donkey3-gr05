using System;
using System.Collections.Generic;

namespace _5002_donkey3_GR05

{
    class Program
    {
        static void KeyInfo()
        {
            ConsoleKeyInfo keyInfo;
            ConsoleKeyInfo keyInfo2;
            Console.WriteLine("");
            Console.WriteLine("Press the <ENTER> key to continue");
            while(keyInfo.Key != ConsoleKey.Enter)
            keyInfo = Console.ReadKey(); 
            Console.Clear();
            keyInfo = keyInfo2;
        }
        // display menu for all the options
        static void Menu()
        {            
            Console.WriteLine("* * * * * * * * * * * * * * * * *");
            Console.WriteLine("*  THE AMAZING MODULAR PROGRAM  *");
            Console.WriteLine("*_______________________________*");
            Console.WriteLine("*                               *");
            Console.WriteLine("*   Please select a number      *");
            Console.WriteLine("*                               *");
            Console.WriteLine("*   1. Multiplication Table     *");
            Console.WriteLine("*   2. Add 5 numbers together   *");
            Console.WriteLine("*   3. Months with 31 days      *");
            Console.WriteLine("*   4. List of Birth Months     *");
            Console.WriteLine("*                               *");
            Console.WriteLine("*   5. Quit                     *");
            Console.WriteLine("*                               *");
            Console.WriteLine("* * * * * * * * * * * * * * * * *");                
        }

        private static void Q1()
        {
            // display question to option 1
            // read inputed number, enter it into an if stament to make sure it is a int or double  
            // then multiply for 1-12
            string sAnswer;
            Console.Clear();
            do 
            {            
                Console.WriteLine("What number would you like to see multiplied? (Type exit to return to menu)");
                sAnswer = Console.ReadLine();            
                double Answer;
                
                if (double.TryParse(sAnswer, out Answer))
                {
                    Console.WriteLine($"{Answer} X 1 = {Answer*1}");
                    Console.WriteLine($"{Answer} X 2 = {Answer*2}");
                    Console.WriteLine($"{Answer} X 3 = {Answer*3}");
                    Console.WriteLine($"{Answer} X 4 = {Answer*4}");
                    Console.WriteLine($"{Answer} X 5 = {Answer*5}");
                    Console.WriteLine($"{Answer} X 6 = {Answer*6}");
                    Console.WriteLine($"{Answer} X 7 = {Answer*7}");
                    Console.WriteLine($"{Answer} X 8 = {Answer*8}");
                    Console.WriteLine($"{Answer} X 9 = {Answer*9}");
                    Console.WriteLine($"{Answer} X 10 = {Answer*10}");
                    Console.WriteLine($"{Answer} X 11 = {Answer*11}");
                    Console.WriteLine($"{Answer} X 12 = {Answer*12}");
                    Console.WriteLine("");

                    Console.WriteLine("Press the <ENTER> key to continue");
                    ConsoleKeyInfo keyInfo;
                    while(keyInfo.Key != ConsoleKey.Enter)
                    keyInfo = Console.ReadKey(); 
                    Console.Clear();
                    ConsoleKeyInfo keyInfo2;
                    keyInfo = keyInfo2;
                } 
                else if ((sAnswer == "exit") || (sAnswer == "Exit"))
                {
                }
                else 
                {
                    Console.Clear();
                    Console.WriteLine("Please Enter a number");
                }

            } while((sAnswer != "exit") && (sAnswer != "Exit"));
        }

        private static void Q2()
        {   // Display qusetions for user to enter all numbers and enter into list
            // Add numbers into list and display total
            var stuff = new List<int>{};
            System.Console.WriteLine("plese enter your first number");
            var read1 =  int.Parse(System.Console.ReadLine());
            stuff.Add (read1);

            System.Console.WriteLine("plese enter your second number");
            var read2 = int.Parse(System.Console.ReadLine());
            stuff.Add (read2);

           System.Console.WriteLine("plese enter your third number");
            var read3 = int.Parse(System.Console.ReadLine());
            stuff.Add (read3);

           System.Console.WriteLine("plese enter your forth number");
            var read4 = int.Parse(System.Console.ReadLine());
            stuff.Add (read4);

           System.Console.WriteLine("plese enter your fith number");
            var read5 = int.Parse(System.Console.ReadLine());
            stuff.Add (read5);

            System.Console.WriteLine(string.Join(",", stuff));
            System.Console.WriteLine($"the total of your numbers is {read1 + read2 + read3 + read4 + read5}");
        }
        private static void Q3()
        {
            // Dictionary of the months and days, display months with 31 days
            Console.Clear();
            var months = new Dictionary<string, int>();

            months.Add("January", 31);
            months.Add("February", 28);
            months.Add("March", 31);
            months.Add("April", 30);
            months.Add("May", 31);
            months.Add("June", 30);
            months.Add("July", 31);
            months.Add("August", 31);
            months.Add("September", 30);
            months.Add("October", 31);
            months.Add("November", 30);
            months.Add("December", 31);

            foreach (var x in months)
            {
                if (x.Value == 31)
                {
                    Console.WriteLine($"{x.Key} has only {x.Value} days");
                }
            }
        }

        private static void Q4()
        {
            // List holds names and birth months
            // display in order of months 
            Console.Clear();
            List<Persons> persons = new List<Persons>();

            persons.Add(new Persons("Phillip", "January"));
            persons.Add(new Persons("Tony", "Feburary"));
            persons.Add(new Persons("Clinton", "March"));
            persons.Add(new Persons("Daisy", "April"));
            persons.Add(new Persons("Lucky", "May"));
            persons.Add(new Persons("Rogue", "June"));
            persons.Add(new Persons("Natalia", "July"));
            persons.Add(new Persons("Logan", "September"));
            persons.Add(new Persons("Felicia", "October"));
            persons.Add(new Persons("Xavier", "December"));
            
            foreach(Persons Persons in persons)
            {
                Console.WriteLine($"My name is {Persons.Name} I was born in {Persons.Month}.");
            }
        }
        
        static void Main(string[] args)

        {
            // dispaying all the outcomes of the code blocks above
            Console.Clear();
            int menu;
            do
            {                
                Menu();
                string sMenu = Console.ReadLine();                
                if (int.TryParse(sMenu, out menu))
                {
                    switch (menu)
                    {
                        case 1:
                            Q1();
                            Console.Clear();
                            Console.WriteLine("Thank you for using our multiplication service");
                            KeyInfo();
                            break;
                        case 2:
                            Q2();
                            KeyInfo();
                            Console.Clear();
                            Console.WriteLine("Thank you for using our 5 number calculator service");
                            KeyInfo();
                            break;
                        case 3:
                            Q3();
                            KeyInfo();
                            Console.Clear();
                            Console.WriteLine("Thank you for using our month length service");
                            KeyInfo();
                            break;
                        case 4:
                            Q4();
                            KeyInfo();
                            Console.Clear();
                            Console.WriteLine("Thank you for using our name and birth month service");
                            KeyInfo();
                            break;
                        case 5:
                            Console.Clear();
                            Console.WriteLine("");
                            break;
                        default:
                            Console.Clear();
                            Console.WriteLine("Please enter a valid selection number");
                            break;
                    } 
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("Error. Please type a number to select an option");
                }
            }while (menu != 5);
            Console.WriteLine("Thank you for visiting the amazing modular program, Goodbye");
        }
    }
}
